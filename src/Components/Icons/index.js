import React from 'react';
import { ReactSVG } from 'react-svg';
import instagram from './resources/icon-instagram.svg';
import facebook from './resources/icon-facebook.svg';
import down from './resources/icon-down.svg';
import up from './resources/icon-up.svg';
import twitter from './resources/icon-twitter.svg';

const InstagramIcon = ({overview}) => <ReactSVG className={overview ? "overview-icon" : "social-icon"} src={instagram} />;
const IconDown = ({overview}) => <ReactSVG className={overview ? "overview-icon" : "social-arrow-icon"} src={down} />;
const IconUp = ({overview}) => <ReactSVG className={overview ? "overview-icon" : "social-arrow-icon"} src={up} />;
const IconFacebook = ({overview}) => <ReactSVG className={overview ? "overview-icon" : "social-icon"} src={facebook} />;
const IconTwitter = ({overview}) => <ReactSVG className={overview ? "overview-icon" : "social-icon"} src={twitter} />;

export default {
	InstagramIcon,
	IconDown,
	IconUp,
	IconFacebook,
	IconTwitter
}