import React from 'react';
import '../../Services/Styles/switch.css';

const Switch = ({ dispatch, darkmode }) => {

	const switchDarkMode = () => {
		return dispatch({
			type:'SWITCH_DARK_MODE'
		})
	}

	return(
		<div className="switch-block">
			<h1 className={!darkmode ? 'switch-block-title' : ''}>Dark Mode</h1>
			<div className="switch-block-button" onClick={switchDarkMode}>
				<div className={ darkmode ? "switch-block-button-circle switch-block-button-circle-left" : "switch-block-button-circle"}></div>
			</div>
		</div>
	)
}

export default Switch;