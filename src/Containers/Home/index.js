import React from 'react';
import Social from './Helpers/Social';
import Overview from './Helpers/overview';
import { connect } from 'react-redux';

const Home = ({ dispatch, darkmode }) => {

	return(
		<div className={ darkmode ? "main-block" : "main-block main-block-white"}>
			<Social dispatch={dispatch} darkmode={darkmode}/>
			<Overview darkmode={darkmode}/>
		</div>
	)

}

const mapStateToProps = (state) => {
	return {
		darkmode:state.app.darkmode
	}
}

export default connect(mapStateToProps)(Home);