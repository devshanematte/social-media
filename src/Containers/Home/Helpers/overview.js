import React from 'react';
import { constants } from '../../../Domain';
import { Icons } from '../../../Components';
import Item from './item';

const Overview = ({type, darkmode}) => {

	return(
		<div className="container">
			<div className="overview-block-title">
				<h1 className={!darkmode ? 'overview-title-light' : ''}>Overview - Today</h1>
			</div>
			<div className="overview-block">
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="87" social="facebook" ratingStatus="up" rating={3}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="52" social="facebook" ratingStatus="up" rating={2}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="5462" social="instagram" ratingStatus="up" rating={2257}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="52k" social="instagram" ratingStatus="up" rating={1375}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="117" social="twitter" ratingStatus="down" rating={303}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="507" social="twitter" ratingStatus="up" rating={553}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="107" social="instagram" ratingStatus="down" rating={19}/>
				<Item type="OVERVIEW_ITEM" darkmode={darkmode} subscribers="1407" social="instagram" ratingStatus="down" rating={12}/>
			</div>

		</div>
	)

}

export default Overview;