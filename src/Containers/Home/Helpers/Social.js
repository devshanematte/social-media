import React from 'react';
import Item from './item';
import { Switch } from '../../../Components';

const Social = ({dispatch, darkmode}) => {

	return(
		<div className={"social-block"}>
			
			<div className={darkmode ? "social-top" : "social-top social-top-white"}>
				<div className="container">
					<div className="social-top-content">
						<div className="social-top-left">
							<h1 className={!darkmode ? "social-title-white" : ""}>Social Media Dashboard</h1>
							<p className={!darkmode ? "social-description-white" : ""}>Total Followers: 23,004</p>
						</div>
						<div className="social-top-right">
							<Switch darkmode={darkmode} dispatch={dispatch}/>
						</div>
					</div>
				</div>
			</div>

			<div className="container">
				<div className="social-list">
					<Item darkmode={darkmode} type="FACEBOOK"/>
					<Item darkmode={darkmode} type="TWITTER"/>
					<Item darkmode={darkmode} type="INSTAGRAM"/>
					<Item darkmode={darkmode} />
				</div>
			</div>

		</div>
	)

}

export default Social;