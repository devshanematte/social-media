import React from 'react';
import { constants } from '../../../Domain';
import { Icons } from '../../../Components';

const Item = ({
	type,
	subscribers = 0,
	social = 'facebook',
	ratingStatus = 'up',
	rating = 0,
	darkmode = true
}) => {

	switch(type){
		case constants.OVERVIEW_ITEM :
			return (
				<div className={darkmode ? "overview-item" : "overview-item overview-item-white"}>

					<div className="overview-item-top">
						<h3>Page Views</h3>
						{
							social === 'twitter' ?
								<Icons.IconTwitter overview/>
							: social === 'instagram' ?
								<Icons.InstagramIcon overview/>
							: social === 'facebook' ?
								<Icons.IconFacebook overview/>
							: <Icons.InstagramIcon overview/>
						}
					</div>

					<div className="overview-item-bottom">
						<h4 className={!darkmode ? 'subscriber-light' : ''}>{ subscribers }</h4>
						<div className="overview-item-bottom-left">
							{ ratingStatus === 'up' ? <Icons.IconUp/> : <Icons.IconDown/>}
							<p className={ratingStatus === 'up' ? 'rating-status-up' : 'rating-status-down'} >{ rating }%</p>
						</div>

					</div>

				</div>
			)
		case constants.INSTAGRAM :
			return (
				<div className={darkmode ? "social-item" : "social-item social-item-white"}>
					<div className="line-social color-instagram"></div>
					
					<div className="social-item-link">
						<Icons.InstagramIcon/>
						<a href="#">@realnathanf</a>
					</div>

					<div className="social-item-num">
						<h2 className={!darkmode ? 'subscriber-light' : ''}>11k</h2>
						<h4>followers</h4>
					</div>

					<div className="social-item-rating">
						<Icons.IconUp/>
						<p>1099 Total</p>
					</div>

				</div>
			)
		case constants.FACEBOOK :
			return (
				<div className={darkmode ? "social-item" : "social-item social-item-white"}>
					<div className="line-social color-facebook"></div>
	
					<div className="social-item-link">
						<Icons.IconFacebook/>
						<a href="#">@nathanf</a>
					</div>

					<div className="social-item-num">
						<h2 className={!darkmode ? 'subscriber-light' : ''}>1987</h2>
						<h4>followers</h4>
					</div>

					<div className="social-item-rating">
						<Icons.IconUp/>
						<p>12 Total</p>
					</div>

				</div>
			)
		case constants.TWITTER :
			return (
				<div className={darkmode ? "social-item" : "social-item social-item-white"}>
					<div className="line-social color-twitter"></div>

					<div className="social-item-link">
						<Icons.IconTwitter/>
						<a href="#">@nathanf</a>
					</div>

					<div className="social-item-num">
						<h2 className={!darkmode ? 'subscriber-light' : ''}>1044</h2>
						<h4>followers</h4>
					</div>

					<div className="social-item-rating">
						<Icons.IconUp/>
						<p>99 Total</p>
					</div>

				</div>
			)
		default :
			return (
				<div className={darkmode ? "social-item" : "social-item social-item-white"}>
					<div className="line-social color-default"></div>

					<div className="social-item-link">
						<Icons.InstagramIcon/>
						<a href="#">@nathanf</a>
					</div>

					<div className="social-item-num">
						<h2 className={!darkmode ? 'subscriber-light' : ''}>8239</h2>
						<h4>followers</h4>
					</div>

					<div className="social-item-rating">
						<Icons.IconDown/>
						<p className="down">144 Total</p>
					</div>

				</div>
			)
	}

}

export default Item;