import React from 'react';
import { Provider } from 'react-redux';
import { Store } from '../Services';
import { PersistGate } from 'redux-persist/lib/integration/react';

import Home from './Home';

const App = () => {

	const {
		store,
		persistor
	} = Store();

	return(
		<Provider store={store}>
			<PersistGate persistor={persistor}>
				<Home/>
			</PersistGate>
		</Provider>
	)
}

export default App;