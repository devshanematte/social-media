const initialState = {
	version:'0.0.1',
	darkmode:true
}

const app = (state = initialState, action: any) => {
	switch (action.type){
		case 'SWITCH_DARK_MODE' :
			return {
				...state,
				darkmode:!state.darkmode
			}
		default :
			return state
	}
}

export default app